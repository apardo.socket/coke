package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/apardo.socket/coke/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
